'use strict';
const Generator = require('yeoman-generator');
const chalk = require('chalk');
const yosay = require('yosay');
const path = require('path');

module.exports = class extends Generator {
  prompting() {
    // Have Yeoman greet the user.
    this.log(yosay(
      'Welcome to the ' + chalk.red('generator-colramlapi:notes') + ' generator!'
));

    const prompts = [{
      type: 'input',
      name: 'fn',
      message: 'Filename (NOTE: .md will be appended): '
    },
    {
        type: 'input',
        name: 'title',
        message: 'What is the title?'
    }]
    return this.prompt(prompts).then(props => {
        this.props = props;
    });

  }

  writing() {
    this.fs.copyTpl(
      this.templatePath('_notes.md'),
      this.destinationPath(path.join('documentation', (this.props.fn + '.md'))),
        { title: this.props.title});
  }

  install() {
  }
};
