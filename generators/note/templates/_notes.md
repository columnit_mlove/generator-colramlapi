# <%= title %>
### Use Case Overview:

### Source System:


### Target System


### Mapping Requirements:
#### Asset Types:


| Source Column | Source Table | Destination Column | Destination Table | Notes |
| ------------- | ------------- | ------------------ | ----------------- | ------ |
| | | | | |

#### Example

### Transport Protocol:
    
### Data format:

#### Response:
    
##### Example:


### Transformation requirements:

### Synchronous or Asynchronous processing:

### Batch or real-time:

### Transaction requirement:

### Error handling requirements:

### Logging requirements:

### Failure Alert requirements:

### Security Requirements:

