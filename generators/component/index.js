'use strict';
const Generator = require('yeoman-generator');
const chalk = require('chalk');
const yosay = require('yosay');
const mkdirp = require('mkdirp');
const path = require('path');

function dirJsonTraverse(jsonObj, root){
    if (typeof jsonObj == "object") {
                
        Object.keys(jsonObj).forEach(function(k){
            console.log('creating ' + root + '/' + k)                       
            mkdirp(path.join(root, k));
            dirJsonTraverse(jsonObj[k], path.join(root,k));
        });
    }
}

module.exports = class extends Generator {
  prompting() {
    this.log(yosay(
      'Lets generate some RAML 1.0 formatted components!'
    ));

    const prompts = [{
      type: 'list',
      name: 'type',
      message: 'What type of RAML component are you trying to create?',
        choices: ["annotation", "example", "library", "overlay", "resourceType",
            "securityScheme", "traits", "types"]
    },
    {
        when: function(answers){ 
                return ['annotation', 'resourceType'].includes(answers.type)},
        type: 'input',
        name: 'title',
        message: 'What is the document title?'
    },
    {
        when: function(answers){ 
                return ['overlay'].includes(answers.type)},
        type: 'input',
        name: 'extends',
        message: 'What does the document extend?'
    },
    {
        type: 'input',
        name: 'fn',
        message: 'What should we call the file (no need to include extension)?'
    },
    {
        type: 'input',
        name: 'dir',
        message: 'Where is the document going (project directory)?' 
    }
    ];

    return this.prompt(prompts).then(props => {
      // To access props later use this.props.someAnswer;
      this.props = props;
    });
  }

  writing() {

    var subs = {}; 
    if ('title' in this.props) {
        subs['title'] = this.props.title
    }

    if ('extends' in this.props) {
        subs['extends'] = this.props.extends
    }
     
    this.fs.copyTpl(
        this.templatePath('_' + this.props.type + '.tmp'),
        this.destinationPath(path.join(this.props.dir, 
                (this.props.fn + (function(type){
                    if (type == 'example'){
                        return ".json"
                    } 
                    return ".raml"
                })(this.props.type)
                )
            )
        ),
       subs
    );

  }

  install() {
  }
};
