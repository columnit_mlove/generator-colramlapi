'use strict';
const Generator = require('yeoman-generator');
const chalk = require('chalk');
const yosay = require('yosay');
const mkdirp = require('mkdirp');
const path = require('path');

function dirJsonTraverse(jsonObj, root){
    if (typeof jsonObj == "object") {
                
        Object.keys(jsonObj).forEach(function(k){
            console.log('creating ' + root + '/' + k)                       
            mkdirp(path.join(root, k));
            dirJsonTraverse(jsonObj[k], path.join(root,k));
        });
    }
}

module.exports = class extends Generator {
  prompting() {
    this.log(yosay(
      'Welcome to the ' + chalk.red('generator-colramlapi') + ' generator!'
    ));

    const prompts = [{
      type: 'confirm',
      name: 'api',
      message: 'Would you like to create an api.raml file?',
      default: true
    },
    {
        type: 'input',
        name: 'title',
        message: 'What is the project title?',
        store: true
    }
    {
        type: 'confirm',
        name: 'git',
        message: 'Run git init?'
    }
    ];

    return this.prompt(prompts).then(props => {
      // To access props later use this.props.someAnswer;
      this.props = props;
    });
  }

  writing() {
    const dirs = this.fs.readJSON(this.templatePath('dir.json'));
    this.log("Creating directory structure for RAML project");
    dirJsonTraverse(dirs['root'], this.destinationPath(''));
    if (this.props.api){
        this.log('Creating: '+  this.destinationPath('') + '/' + 'api.raml');
        
        this.fs.copyTpl(
          this.templatePath('_api.raml'),
          this.destinationPath('api.raml'),
          { title: this.props.title }
        );
    
    }

    
    this.fs.copyTpl(
      this.templatePath('_README.md'),
      this.destinationPath('README.md'),
      { title: props.title }
    );
  }

  install() {
    if (this.props.git){
        this.spawnCommand('git', ['init', '-q']);
    }
  }
};
