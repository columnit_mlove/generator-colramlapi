# Yeoman Generator for Raml API Projects

## Purpose
Generator for structuring RAML based api projects.  This should provide all of the standard template
generations for any RAML api project.



## Installation
### Pre-reqs
Download and install node, npm, and yo
### Install

1. Clone the repository to a directory.  
2. Download and install node, npm
3. In the cloned repo's root, run npm link

## Use
### yo colramlapi

Generate a RAML project with the directory structure

```
.
|--documentation
|--examples
|--libraries
|--notebooks
|--resourceTypes
|--schemas
|--scripts
|--securitySchemes
|--traits
|--README.md
|--api.raml

```

### yo colramlapi:note

Generate a md formatted note for collecting information/requirements for 
an api or an integration. 

### yo colramlapi:component
Generate a raml component file; supported types are:
- annotation
- example
- library
- overlay
- resourceType
- securityScheme
- traits
- types

Information about each of these types can be found in the RAML specification:
https://github.com/raml-org/raml-spec/blob/master/versions/raml-10/raml-10.md/
